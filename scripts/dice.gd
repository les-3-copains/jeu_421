extends Node3D

var velocity = 0

# Called when the node enters the scene tree for the first time.
var rng = RandomNumberGenerator.new()

func _ready():
	await get_tree().create_timer(0.1).timeout

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Input.is_action_just_pressed("ui_space"):
		Globals.DICE_VALUES = ['0', '0', '0']
		Globals.GAME_STARTED = true;
		throw_dice()

func throw_dice():
	var target_position = Vector3(0, 0, 0)
	var random_x = rng.randf_range(-8.0, 8.0)
	var random_y = rng.randf_range(5.0, 15.0)
	var random_z = rng.randf_range(-8.0, 8.0)
	var random_rotation = Vector3(rng.randi_range(0, 360), rng.randi_range(0, 360), rng.randi_range(0, 360))
	var random_force = Vector3(rng.randi_range(-5, 5), rng.randi_range(0, -20), rng.randi_range(-5, 5))
	var current_position = Vector3(random_x, random_y, random_z)
	var direction = (target_position - current_position).normalized()
	rotation = random_rotation
	$PhysicalDice.constant_force = random_force
	$PhysicalDice.global_position = current_position
	$PhysicalDice.apply_central_impulse(direction)
