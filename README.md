# Jeu_421



## Installation

    Vous avez juste à exécuter l'exécutable dans le répertoire "Executables" pour lancer l'application.


## Demo

    Gameplay Single Player : https://youtu.be/rzfxkh_IhVw 
    Gameplay Multi Player : https://youtu.be/SI1QgKC8vvw

## Explication : 

- Les dés se relances si ils atteingnent la valeur 6.
- Pour simplifier le développement j'ai fait en sorte d'attendre 3 secondes après le lancement pour détecter la valeur du dé.
- Sur les démos il n'y a pas encore de colisions autour du terrain.
- Les dés se relancent aussi lorsqu'ils sont pas considéré comme étant sur le sol, ce qui arrive parfois alors qu'ils sont bien visuellement sur le sol, dans ce cas ils sont relancés ou non compté dans le score.

