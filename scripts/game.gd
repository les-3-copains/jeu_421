extends Node3D

@onready
var throw_label = $Labels/Throws/Counter
@onready
var values_label = $Labels/Values/Counter
@onready
var turns_label = $Labels/Turns/Counter
@onready
var player_label = $Labels/Player/PlayerName

# Called when the node enters the scene tree for the first time.
func _ready():
	$EndScreen.visible = false
	$Labels/Player.visible = Globals.IS_MULTI
	turns_label.text = str(Globals.NUMBER_TURNS)
	throw_label.text = str(Globals.NUMBER_THROWS)
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Input.is_action_just_pressed("ui_r"):
		Globals.IS_MULTI = false
		Globals.NUMBER_TURNS = 0
		Globals.END_GAME = false
		Globals.DICE_VALUES = ['0', '0', '0']
		Globals.GAME_STARTED = false
		throw_label.text = '0'
		values_label.text = '0'
		turns_label.text = '0'
		change_scene("start_screen")
		hide()
		
	if Input.is_action_just_pressed("ui_space"):
		if Globals.END_GAME == true:
			return
		Globals.NUMBER_THROWS += 1
		throw_label.text = str(Globals.NUMBER_THROWS)
		var counter = int(throw_label.text) + 1
		throw_label.text = str(counter)
		
		if Globals.IS_MULTI == false:
			Globals.NUMBER_TURNS = counter
			turns_label.text = str(Globals.NUMBER_TURNS)
		else:
			if counter % 2 != 0:
				Globals.SCORE_PLAYER_B = int(Globals.DICE_VALUES[0]) + int(Globals.DICE_VALUES[1]) + int(Globals.DICE_VALUES[2])
				player_label.text = Globals.PLAYER_NAME_A
			else: 
				Globals.NUMBER_TURNS = Globals.NUMBER_TURNS + 1
				Globals.SCORE_PLAYER_A = int(Globals.DICE_VALUES[0]) + int(Globals.DICE_VALUES[1]) + int(Globals.DICE_VALUES[2])
				player_label.text = Globals.PLAYER_NAME_B
				turns_label.text = str(Globals.NUMBER_TURNS)
				
	if Globals.END_GAME == true:
		if Globals.IS_MULTI == false:
			$EndScreen/Win_Label.text = "GG ta gagner mn fratté"
		else:
			$EndScreen/Win_Label.text = "Gagnage du joueur {playerName}".format({"playerName": $Labels/Player/PlayerName.text})
		$EndScreen.visible = true
	values_label.text = "{1}, {2}, {3}".format(
		{
			"1": Globals.DICE_VALUES[0], 
			"2": Globals.DICE_VALUES[1], 
			"3": Globals.DICE_VALUES[2]
		}
	)
	
func _on_area_button_retry_input_event(_camera, event, _position, _normal, _shape_idx):
	if event is InputEventMouseMotion:
		if Input.is_action_just_pressed('ui_mouse_left_click'):
			$EndScreen/NodeReTry/Button/Area_Button_Retry/Hitbox.disabled = true
			throw_label.text = '0'
			values_label.text = '0'
			turns_label.text = '0'
			$EndScreen.visible = false
			Globals.END_GAME = false
			Globals.IS_MULTI = false
			Globals.GAME_STARTED = false
			Globals.DICE_VALUES = ['0', '0', '0']
			change_scene("start_screen")
			hide()

func change_scene(l_scene):
	# Queue the current scene to free on the next frame:
	var root_node = get_tree().get_root()
	var scene_node = root_node.get_node("Game")
	scene_node.queue_free()
	var scene =  "res://scenes/{scene}.tscn".format({"scene": l_scene})
	var new_scene_resource = load(scene) # Load the new level from disk
	var new_scene_node = new_scene_resource.instantiate() # Create an actual node of it for the game to use
	root_node.add_child(new_scene_node) # Add to the tree so the level starts processing
