extends Node3D

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func _on_area_button_mono_input_event(_camera, event, _position, _normal, _shape_idx):
	if event is InputEventMouseMotion:
		if Input.is_action_just_pressed('ui_mouse_left_click'):
			$NodeStart/ButtonSingle/Area_Button_Mono/Hitbox.disabled = true
			load_save()
			change_scene("game")

func _on_area_button_multi_input_event(_camera, event, _position, _normal, _shape_idx):
	if event is InputEventMouseMotion:
		if Input.is_action_just_pressed('ui_mouse_left_click'):
			$NodeStart/ButtonMulti/Area_Button_Multi/Hitbox.disabled = true
			load_save()
			change_scene("game")

func change_scene(l_scene):
	# Queue the current scene to free on the next frame:
	var root_node = get_tree().get_root()
	var scene_node = root_node.get_node("StartScreen")
	scene_node.queue_free()
	var scene =  "res://scenes/{scene}.tscn".format({"scene": l_scene})
	var new_scene_resource = load(scene) # Load the new level from disk
	var new_scene_node = new_scene_resource.instantiate() # Create an actual node of it for the game to use
	root_node.add_child(new_scene_node) # Add to the tree so the level starts processing

func load_save():
	if not FileAccess.file_exists("user://savegame.save"):
		return
	var save_game = FileAccess.open("user://savegame.save", FileAccess.READ)
	while save_game.get_position() < save_game.get_length():
		var json_string = save_game.get_line()
		var json = JSON.new()
		var parse_result = json.parse(json_string)
		if not parse_result == OK:
			print("JSON Parse Error: ", json.get_error_message(), " in ", json_string, " at line ", json.get_error_line())
			continue
		var json_data = json.get_data()
		Globals.IS_MULTI = json_data['IS_MULTI'] if json_data['IS_MULTI'] else false
		Globals.END_GAME = json_data['END_GAME'] if json_data['END_GAME'] else false
		Globals.DICE_VALUES = json_data['DICE_VALUES'] if json_data['DICE_VALUES'] else ['0', '0', '0']
		Globals.GAME_STARTED = json_data['GAME_STARTED'] if json_data['GAME_STARTED'] else false
		Globals.NUMBER_TURNS = json_data['NUMBER_TURNS'] if json_data['NUMBER_TURNS'] else 0
		Globals.NUMBER_THROWS = json_data['NUMBER_THROWS'] if json_data['NUMBER_THROWS'] else 0
		Globals.PLAYER_NAME_A = json_data['PLAYER_NAME_A'] if json_data['PLAYER_NAME_A'] else 'A'
		Globals.PLAYER_NAME_B = json_data['PLAYER_NAME_B'] if json_data['PLAYER_NAME_B'] else 'B'
		Globals.SCORE_PLAYER_A = json_data['SCORE_PLAYER_A'] if json_data['SCORE_PLAYER_A'] else 0
		Globals.SCORE_PLAYER_B = json_data['SCORE_PLAYER_B'] if json_data['SCORE_PLAYER_B'] else 0
