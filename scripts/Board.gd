extends Node3D

var score = 0
var win_cond = [1, 2, 4]
var childs_value = []

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Input.is_action_just_pressed("ui_space"):
		$Area/Hitbox.disabled = false

func _on_area_body_entered(body):
	if Globals.GAME_STARTED == false:
		return null
	var childrens = body.get_children()
	await get_tree().create_timer(3).timeout
	for children in childrens:
		var child_pos = children.global_position
		if child_pos.y > 1:
			continue
		var dice_value = get_value_from_name(children.name)
		if dice_value == 6:
			body.get_parent().get_parent().throw_dice()
		else:
			childs_value.push_back(dice_value)

	if childs_value.size() >= 3:
		Globals.DICE_VALUES = [childs_value[0], childs_value[1], childs_value[2]]
		Globals.END_GAME = contains_elements(childs_value, win_cond)
		childs_value = []
		$Area/Hitbox.disabled = true
	save()

func contains_elements(arr, elements):
	for element in elements:
		if not element in arr:
			return false
	return true
	
func get_value_from_name(l_name):
	return int(l_name.trim_prefix("Face_"))


func _on_bounds_body_exited(body):
	if Globals.GAME_STARTED == false:
		return null
	if body.get_parent().get_parent().has_method("throw_dice"):
		body.get_parent().get_parent().throw_dice()

func save():
	var save_game = FileAccess.open("user://savegame.save", FileAccess.WRITE)
	print("save : ", save_game)
	var save_dict = {
		'IS_MULTI': Globals.IS_MULTI,
		'END_GAME': Globals.END_GAME,
		'DICE_VALUES': Globals.DICE_VALUES,
		'GAME_STARTED': Globals.GAME_STARTED,
		'NUMBER_TURNS': Globals.NUMBER_TURNS,
		'NUMBER_THROWS': Globals.NUMBER_THROWS,
		'PLAYER_NAME_A': Globals.PLAYER_NAME_A,
		'PLAYER_NAME_B': Globals.PLAYER_NAME_B,
		'SCORE_PLAYER_A': Globals.SCORE_PLAYER_A,
		'SCORE_PLAYER_B': Globals.SCORE_PLAYER_B,
	}
	var json_string = JSON.stringify(save_dict)
	save_game.store_line(json_string)
	
	return save_dict
